import random
CONSONANTES = "bcdfghjklmnprstvwxyz"
VOCALES = "aeiou"


class Gasto:
    def __init__(self, codigo, descripcion, mes, sucursal, importe):
        self.codigo = codigo
        self.descripcion = descripcion
        self.mes = mes
        self.sucursal = sucursal
        self.importe = importe


def write(gasto):
    print("Codigo:", gasto.codigo, end=" - ")
    print("Descripcion:", gasto.descripcion, end=" - ")
    print("Mes:", gasto.mes, end=" - ")
    print("Sucursal:", gasto.sucursal, end=" - ")
    print("Importe:", gasto.importe)


def generar_descripcion():
    desc = ""
    for i in range(3):
        desc += random.choice(CONSONANTES)
        desc += random.choice(VOCALES)
    return desc


def cargar_gasto_random():
    codigo = random.randint(1, 10000)
    descripcion = generar_descripcion()
    mes = random.randint(1, 12)
    sucursal = random.randint(0, 2)
    importe = random.randint(100, 100000) / 100
    return Gasto(codigo, descripcion, mes, sucursal, importe)


if __name__ == '__main__':
    gasto = cargar_gasto_random()
    write(gasto)

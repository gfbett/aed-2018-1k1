import gastos
import pickle
import os

ARCHIVO = "gastos.dat"
    

def menu():
    print("1 _ Cargar gastos")
    print("2 _ Mostrar gastos")
    print("3 _ Generar archivo")
    print("4 _ Mostrar archivo")
    print("5 _ Gastos por mes y sucursal")
    print("6 _ Gastos de un mes")
    print("7 _ Salir")
    return int(input("Ingrese opción: "))


def cargar_gastos(n):
    v = [None] * n
    for i in range(n):
        v[i] = gastos.cargar_gasto_random()
    return v


def mostrar_vector(v):
    for gasto in v:
        gastos.write(gasto)


def generar_archivo(v, x):
    file = open(ARCHIVO, "wb")
    for i in range(len(v)):
        if v[i].importe > x:
            # Grabar registro
            pickle.dump(v[i], file)

    file.close()


def mostrar_archivo():

    file = open(ARCHIVO, "rb")
    tamanio = os.path.getsize(ARCHIVO)
    while file.tell() < tamanio:
        gasto = pickle.load(file)
        gastos.write(gasto)

    file.close()


def generar_matriz():
    mat = [[0] * 12 for i in range(3)]

    file = open(ARCHIVO, "rb")
    tamanio = os.path.getsize(ARCHIVO)
    while file.tell() < tamanio:
        gasto = pickle.load(file)
        mat[gasto.sucursal][gasto.mes - 1] += gasto.importe

    file.close()
    return mat


def main():
    v = []
    op = 0
    while op != 7:
        op = menu()
        if op == 1:
            n = int(input("Ingrese cantidad de registros: "))
            v = cargar_gastos(n)
        elif op == 2:
            mostrar_vector(v)
        elif op == 3:
            x = float(input("Ingrese valor mínimo: "))
            generar_archivo(v, x)
        elif op == 4:
            mostrar_archivo()
        elif op == 5:
            matriz = generar_matriz()
            for i in range(len(matriz)):
                for j in range(len(matriz[0])):
                    if matriz[i][j] != 0:
                        print("Total de gastos de la sucursal", i, "en el mes", j + 1, ":", matriz[i][j])
        elif op == 6:
            pass


if __name__ == '__main__':
    main()

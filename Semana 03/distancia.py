TOTAL = 3641.3 * 1000
distancia = int(input("Ingrese distancia recorrida en metros: "))

kilometros, metros = divmod(distancia, 1000)

# kilometros = distancia // 1000
# metros = distancia % 1000

porcentaje = distancia * 100 / TOTAL

print("Recorrió", kilometros, "Km y", metros, "m.")
print("Representa un", round(porcentaje, 3), "% del objetivo")


import random
from incidentes import *
def menu():
    print('1-Mostrar incidentes con costo superior a')
    print('2-Mostrar incidentes ordenados')
    print('3-Incidentes por tipo')
    print('4-Buscar incidente')
    print('5-Salir')
    op = int(input("Seleccione una opción:"))
    return op

def cargar_incidentes():
    v = []
    n = int(input('Ingrese cantidad de incidentes a procesar: '))
    for i in range(n):
        cod = random.randint(1,1000)
        desc = random.choice(('Falla 1', 'Falla2', 'Falla3', 'Falla4', 'FallaN'))
        tipo = random.randint(0,14)
        costo = random.random()* 10000
        reg = Incidente(cod, desc, tipo, costo)
        v.append(reg)
    return v


def mostrar_incidentes(vec):
    for x in vec:
        write(x)

def incidentes_costo(vec,x):
    v = []
    for inc in vec:
        if inc.costo >= x:
            v.append(inc)
    return v


def ordenar_incidentes(vec):
    n = len(vec)
    for i in range(n-1):
        for j in range(i+1, n):
            if vec[i].codigo > vec[j].codigo:
                vec[i], vec[j] = vec[j], vec[i]


def incidentes_por_tipo(vec):
    v = [0]*15
    for x in vec:
        v[x.tipo] += 1
    return v


def buscar_incidente(vec, x):
    n = len(vec)
    for i in range(n):
        if vec[i].codigo == x:
            return i
    return -1


def calcular_costo_promedio(vec):
    sum = 0
    n = len(vec)
    for i in range(n):
        sum += vec[i].costo

    return sum/n

def main():
    vec = cargar_incidentes()
    op = 0
    while op != 5:
        op = menu()

        if op == 1:
            x = calcular_costo_promedio(vec)
            v = incidentes_costo(vec,x)
            mostrar_incidentes(v)
        elif op == 2:
            ordenar_incidentes(vec)
            mostrar_incidentes(vec)
        elif op == 3:
            tipos = incidentes_por_tipo(vec)
            for i in range(15):
                if tipos[i] != 0:
                    print('Incidente tipo:', i, ':',tipos[i], 'incidentes')
        elif op == 4:
            x = int(input('Ingrese un código de incidente a buscar: '))
            indice = buscar_incidente(vec, x)
            if indice != -1:
                print('Incidente encontrado ***')
                write(vec[indice])
            else:
                print('Incidente NO encontrado!')
        elif op == 5:
            print('Fin de programa')
        else:
            print('Valor incorrecto. Solo valores en [1-5]')



if __name__ == '__main__':
    main()

class Proyecto:
    def __init__(self, id_proyecto, cliente, honorarios, tipo):
        self.id_proyecto = id_proyecto
        self.cliente = cliente
        self.honorarios = honorarios
        self.tipo = tipo


def write(proyecto):
    print("Proyecto id:", proyecto.id_proyecto, end=" ")
    print("Nombre:", proyecto.cliente, end=" ")
    print("Honorarios:", proyecto.honorarios, end=" ")
    print("Tipo: ", proyecto.tipo)


def menu():
    print("1 _ Cargar datos")
    print("2 _ Mostrar datos")
    print("3 _ Honorarios por tipo")
    print("4 _ Listado de proyectos")
    print("5 _ Buscar por cliente")
    print("6 _ Salir")
    return int(input("Ingrese opción: "))


def cargar_proyectos(n):
    v = [None] * n
    for i in range(n):
        id_proyecto = int(input("Ingrese id: "))
        nombre = input("Nombre del cliente: ")
        honorarios = float(input("Ingrese honorarios: "))
        tipo = int(input("Ingrese tipo: "))
        while tipo < 0 or tipo > 14:
            tipo = int(input("Error. Ingrese tipo: "))
        v[i] = Proyecto(id_proyecto, nombre, honorarios, tipo)
    return v


def mostrar_vector(v):
    for proyecto in v:
        write(proyecto)


def honorarios_por_tipo(v):
    totales = [0] * 15
    for i in range(len(v)):
        tipo = v[i].tipo
        totales[tipo] += v[i].honorarios
    return totales


def ordenar(v):
    n = len(v)
    for i in range(n - 1):
        for j in range(i + 1, n):
            if v[i].honorarios > v[j].honorarios:
                v[i], v[j] = v[j], v[i]


def listar(v):
    for proyecto in v:
        if proyecto.tipo != 4:
            write(proyecto)


def buscar(v, nombre):
    for i in range(len(v)):
        if v[i].cliente == nombre:
            return v[i]
    return None


def main():
    op = 9
    while op != 6:
        op = menu()
        if op == 1:
            n = int(input("Ingrese cantidad de proyectos: "))
            v = cargar_proyectos(n)
        elif op == 2:
            mostrar_vector(v)
        elif op == 3:
            totales = honorarios_por_tipo(v)
            for i in range(len(totales)):
                print("Total tipo", i, ":", totales[i])
        elif op == 4:
            ordenar(v)
            listar(v)
        elif op == 5:
            nombre = input("Ingrese nombre de cliente: ")
            proyecto = buscar(v, nombre)
            if proyecto is None:
                print("No se encontró")
            else:
                print("Datos del proyecto:")
                write(proyecto)
        elif op == 6:
            pass


if __name__ == '__main__':
    main()

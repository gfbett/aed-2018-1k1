import random

ESTACIONES = ("Maipú", "Borges", "Libertador", "Anchorena", "Barrancas",
              "San Isidro R", "Punta Chica", "Marina Nueva",
              "San Fernando R", "Canal", "Delta")


def cargar_pasajeros(ida, vuelta):
    print("Viajes de ida: ")
    for i in range(len(ida)):
        # ida[i] = int(input("Pasajeros en estación " + ESTACIONES[i] + ": "))
        ida[i] = random.randint(0, 20)

    print("Viajes de vuelta:")
    for i in range(len(vuelta) - 1, -1, -1):
        # vuelta[i] = int(input("Pasajeros en estación " + ESTACIONES[i] + ": "))
        vuelta[i] = random.randint(0, 20)


def menu():
    print("Menú de opciones:")
    print("1_Mostrar datos")
    print("2_Total de pasajeros")
    print("3_Estación mayor de ida")
    print("4_Estaciones sin pasajeros")
    print("5_Estaciones con mas de ida")
    print("6_Salir")
    return int(input("Ingrese opción: "))


def mostrar_datos(ida, vuelta):
    for i in range(len(ida)):
        estacion = ESTACIONES[i]
        print(estacion, 'Ida:', ida[i], 'Vuelta:', vuelta[i] )


def sumar(v):
    suma = 0
    for x in v:
        suma += x
    return suma


def estacion_mas_pasajeros(ida):
    mayor = ida[0]
    mayor_pos = 0
    for i in range(1, len(ida)):
        if ida[i] > mayor:
            mayor = ida[i]
            mayor_pos = i
    return mayor, mayor_pos


def estaciones_sin_pasajeros(vuelta):
    cantidad = 0
    for pasajeros in vuelta:
        if pasajeros == 0:
            cantidad += 1
    return cantidad


def mostrar_mas_de_ida(ida, vuelta):
    print("Estaciones con mas pasajeros de ida: ")
    for i in range(len(ida)):
        if ida[i] > vuelta[i]:
            print(ESTACIONES[i])


def main():
    n = len(ESTACIONES)
    ida = [0] * n
    vuelta = [0] * n
    cargar_pasajeros(ida, vuelta)
    op = 0
    while op != 6:
        op = menu()
        if op == 1:
            mostrar_datos(ida, vuelta)
        elif op == 2:
            total_ida = sumar(ida)
            total_vuelta = sumar(vuelta)
            print("Hubo", total_ida, "pasajeros a la ida y", total_vuelta, "a la vuelta")
        elif op == 3:
            mayor, pos = estacion_mas_pasajeros(ida)
            print("La estación con mas pasajeros es:", ESTACIONES[pos], "con", mayor, "pasajeros")
        elif op == 4:
            vacias = estaciones_sin_pasajeros(vuelta)
            porc = vacias * 100 / n
            print("Hubo", vacias, "estaciones sin pasajeros, que representa un ", porc, "% del total")
        elif op == 5:
            mostrar_mas_de_ida(ida, vuelta)
        elif op == 6:
            pass
        else:
            print("Opción incorrecta")


if __name__ == '__main__':
    main()

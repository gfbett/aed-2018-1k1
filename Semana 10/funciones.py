import random


def es_par(numero):
    if numero % 2 == 0:
        return True
    else:
        return False


cantidad_pares = 0
n = int(input("Ingrese cantidad de números: "))
for i in range(n):
    x = random.randint(1, 10000)
    if es_par(x):
        cantidad_pares += 1
print("Hubo", cantidad_pares, "números pares")

# Programa de procesamiento de caracteres
def es_separador(letra):
    return letra in (' ', '.')


def es_vocal(letra):
    """
        Función que determina si una letra es vocal
        :param letra letra a procesar
    """
    if letra in ('a', 'e', 'i', 'o', 'u'):
        return True
    return False


# 1_ Inicialización
clet = palabras_consonante_vocal = palabras_menos_4 = cpal = c_palabras_li = 0
empieza_consonante = termina_vocal = False
aviso_l = False
aviso_li = False

# texto = input("Ingrese texto, terminado en .")
texto = "balon banana tan alta ali li ajonjoli alelili."

for letra in texto:
    if not es_separador(letra):
        # 2_ Dentro de una palabra
        clet += 1
        if clet == 1 and not es_vocal(letra):
            empieza_consonante = True
        termina_vocal = False
        if es_vocal(letra):
            termina_vocal = True
        if letra == 'l' and clet>=3:
            aviso_l = True
        else:
            if aviso_l and letra =='i':
                aviso_li = True
            aviso_l=False

    else:
        # 3_ Fin de palabra
        cpal += 1
        if empieza_consonante and termina_vocal:
            palabras_consonante_vocal += 1
        if clet < 4:
            palabras_menos_4 += 1
        if aviso_li:
            c_palabras_li+=1
        clet = 0
        empieza_consonante =  aviso_l=aviso_li=False

# 4_ Fin de texto
print(texto)
print("Cantidad de palabras que empiezan por consonante y terminan vocal:", palabras_consonante_vocal)

porcentaje = palabras_menos_4 * 100 / cpal
print("Hay", palabras_menos_4, "palabras con menos de 4 letras")
print("Que representan el", porcentaje, "% del total de", cpal, "palabras")
print("Hay", c_palabras_li, "palabras con secuencia li a partir de la 3er letras")

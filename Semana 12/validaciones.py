def sumar(*args):
    suma = 0
    for i in args:
        suma += i
    return suma

def ingresar_rango(desde, hasta, mensaje="Ingrese un número: ",
                   mensaje_error="Error. Intente nuevamente: "):
    x = int(input(mensaje))
    while x < desde or x > hasta:
        x = int(input(mensaje_error))
    return x


# cantidad = ingresar_rango(0, 100, "Ingrese un número entre 0 y 100: ", "No sea zapallo: ")
# print("Se ingresó:", cantidad)
# cantidad2 = ingresar_rango(10, 20, mensaje_error="ERRROOOORRR!!!: ")
# print("Se ingresó:", cantidad2, end=' ', sep='===>')
print(sumar())
print(sumar(2, 2))
print(sumar(2, 2, 4))
print(sumar(2, 2, 4, 5))
print(sumar(2, 2, 4, 5, 6))
print(sumar(2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2))
print("N","N","N","N","N","N","N", sep="a ", end="Batman" )

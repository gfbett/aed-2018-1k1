def cargar_pasajeros(pasajeros):
    for i in range(len(pasajeros)):
        for j in range(len(pasajeros[i])):
            pasajeros[i][j] = int(input("Ingrese cantidad de pasajeros: "))


def totales_linea(pasajeros, n, m):
    totales = [0] * n
    for i in range(n):
        acum = 0
        for j in range(m):
            acum += pasajeros[i][j]
        totales[i] = acum
    return totales


def promedio_por_parada(pasajeros, n, m, x):
    suma = 0
    for i in range(n):
        suma += pasajeros[i][x]
    return suma/n


def menor_pasajeros_por_parada(pasajeros, n, m, x):
    menor = 0
    for i in range(n):
        if i == 0 or pasajeros[i][x] < menor:
            menor = pasajeros[i][x]

    return menor


def calcular_recaudacion(pasajeros, n, m):
    total_pasajeros = 0
    for i in range(n):
        for j in range(m):
            total_pasajeros += pasajeros[i][j]
    return total_pasajeros * 8.5


def main():
    n = int(input("Ingrese cantidad de lineas: "))
    m = int(input("Ingrese cantidad de paradas: "))
    pasajeros = [[0] * m for i in range(n)]

    cargar_pasajeros(pasajeros)
    totales = totales_linea(pasajeros, n, m)
    for i in range(n):
        print("Pasajeros de la línea", i, ":", totales[i])

    x = int(input("Ingrese la parada: "))
    promedio = promedio_por_parada(pasajeros, n, m, x)
    print("Promedio de pasajeros en la parada:", promedio)

    x = int(input("Ingrese la parada: "))
    menor = menor_pasajeros_por_parada(pasajeros, n, m, x)
    print("Menor cantidad de pasajeros en la parada:", menor)

    recaudacion = calcular_recaudacion(pasajeros, n, m)
    print("La recaudación de la empresa fue de $", recaudacion)



if __name__ == '__main__':
    main()

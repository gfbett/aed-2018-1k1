class Disco:
    def __init__(self, titulo, artista, anio, genero, reproducciones):
        self.titulo = titulo
        self.artista = artista
        self.anio = anio
        self.genero = genero
        self.reproducciones = reproducciones


def write(disco):
    print("Disco - Titulo:{} Artista:{} Año:{} Genero:{} Reproducciones:{}"
          .format(disco.titulo, disco.artista, disco.anio,
                  disco.genero, disco.reproducciones)
          )


def cargar_disco():
    titulo = input("Ingrese titulo: ")
    artista = input("Ingrese artista: ")
    anio = int(input("Ingrese año: "))
    genero = int(input("Ingrese género: "))
    reproducciones = int(input("Ingrese reproducciones: "))
    return Disco(titulo, artista, anio, genero, reproducciones)

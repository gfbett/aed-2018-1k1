import random


class Venta:
    def __init__(self, nro, sucursal, vendedor, monto):
        self.nro = nro
        self.sucursal = sucursal
        self.vendedor = vendedor
        self.monto = monto


def cargar_venta_random():
    nro = random.randint(1, 10000)
    sucursal = random.randint(0, 4)
    vendedor = random.randint(1, 10)
    monto = random.randint(100, 10000) / 100
    return Venta(nro, sucursal, vendedor, monto)


def validar_rango(mensaje, valor_min, valor_max):
    x = int(input(mensaje))
    while x < valor_min or x > valor_max:
        print("ERROR!")
        x = int(input(mensaje))
    return x


def cargar_venta():
    nro = int(input("Ingrese nro de venta: "))
    sucursal = validar_rango("Ingrese sucursal: ", 0, 4)
    vendedor = validar_rango("Ingrese vendedor: ", 1, 10)
    monto = float(input("Ingrese monto: "))
    while monto < 0:
        monto = float(input("Error. Ingrese monto: "))

    return Venta(nro, sucursal, vendedor, monto)


def write(venta):
    print("Nro: {} Sucursal: {} Vendedor: {} Monto: {}".format(
        venta.nro,
        venta.sucursal,
        venta.vendedor,
        venta.monto
    ))


if __name__ == '__main__':
    x = cargar_venta_random()
    write(x)

import ventas


def generar_vector():
    v = [None] * 10
    for i in range(len(v)):
        v[i] = ventas.cargar_venta()
    return v


def total_vendedor_sucursal(v):
    columnas = 10
    filas = 5
    cont = [[0] * columnas for i in range(filas)]

    for venta in v:
        sucursal = venta.sucursal
        vendedor = venta.vendedor
        monto = venta.monto
        cont[sucursal][vendedor-1] += monto

    return cont


def cantidad_por_sucursal(v):
    cont = [0] * 5
    for i in range(len(v)):
        venta = v[i]
        cont[venta.sucursal] += 1
    return cont


def mostrar_vector(v):
    for venta in v:
        ventas.write(venta)


def ordenar_nro(v):
    n = len(v)
    for i in range(n-1):
        for j in range(i+1, n):
            if v[i].nro > v[j].nro:
                v[i], v[j] = v[j], v[i]


def buscar(v, x):
    izq = 0
    der = len(v)-1

    while izq <= der:
        c = (izq + der) // 2
        if x == v[c].nro:
            return v[c]
        elif x < v[c].nro:
            der = c - 1
        else:
            izq = c + 1
    return None


if __name__ == '__main__':
    v = generar_vector()
    ordenar_nro(v)
    mostrar_vector(v)
    totales = total_vendedor_sucursal(v)
    print(totales)
    cantidad = cantidad_por_sucursal(v)
    print(cantidad)
    x = int(input("Ingrese numero de venta: "))
    res = buscar(v, x)
    if res is not None:
        print("Datos de la venta: ")
        ventas.write(res)
    else:
        print("No se encontró")

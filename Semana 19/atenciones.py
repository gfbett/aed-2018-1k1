__author__ = 'MARTIN'

def validar(inf, mensaje, sup = None):
    n = inf - 1
    while n < inf or (sup != None and n>sup):
        n = int(input(mensaje))
        if n < inf or (sup != None and n>sup):
            print('Valor incorrecto!')
    return n

def cargar_atenciones(n, practicas, sucursales, montos):
    for i in range(n):
        p = validar(0, 'Ingrese código de práctica [0-9]: ', 9)
        s = validar(0, 'Ingrese código de sucursal [0-5]: ', 5)
        m = validar(0, 'Ingrese monto en $: ')
        practicas[i] = p
        sucursales[i] = s
        montos[i] = m

def montos_sucursal(n, sucursales, montos):
    aux = [0] * 6
    for i in range(n):
        aux[sucursales[i]] += montos[i]
    return aux

def practica_frecuente(n, practicas):
    aux = [0] * 10
    for i in range(n):
        aux[practicas[i]] += 1

    may = aux[0]
    ind = 0
    for i in range(1,10):
        if aux[i]> may:
            may = aux[i]
            ind = i

    return (ind, may)

def conteo_suc_practicas(n, practicas, sucursales):
    mat = [[0] * 10 for i in range(6)]
    for i in range(n):
        fila = sucursales[i]
        columna = practicas[i]
        mat[fila][columna] += 1
    return mat

def main():
    n = validar(0, 'Ingrese cantidad de atenciones a procesar: ')
    practicas = [0]* n
    sucursales = [0]* n
    montos = [0]* n
    # punto 1: carga de vectores
    cargar_atenciones(n, practicas, sucursales, montos)

    # punto 2: Totales cobrados por sucursal (vector de acumulación)
    acu_montos = montos_sucursal(n,sucursales,montos)
    print('Montos acumulados por sucursal:',  acu_montos)

    # punto 3: Práctica más frecuente (vector de conteo y búsqueda del mayor)
    frec, cant = practica_frecuente(n, practicas)
    print('Práctica más frecuente:', frec, ' con', cant, ' veces.')

    # punto 4: Conteo de prácticas por sucursal (Matriz de conteo)
    mat = conteo_suc_practicas(n, practicas, sucursales)
    for f in range(6):
        for c in range(10):
            if mat[f][c] != 0:
                print('Práctica:', f, ' | Sucursal:', c, ' atenciones')

if __name__ == '__main__':
    main()


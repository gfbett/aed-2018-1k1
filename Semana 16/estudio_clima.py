def cargar_datos(temperaturas, regiones, dias, n):
    for i in range(n):
        temperaturas[i] = float(input("Ingrese temperatura: "))
        regiones[i] = int(input("Ingrese región: "))
        dias[i] = int(input("Ingrese día de la muestra: "))


def calcular_promedio(temperaturas):
    n = len(temperaturas)
    suma = 0
    for temp in temperaturas:
        suma += temp
    return suma / n


def ordenar(temperaturas, regiones, dias):
    n = len(temperaturas)
    for i in range(n - 1):
        for j in range(i + 1, n):
            if temperaturas[i] > temperaturas[j]:
                temperaturas[i], temperaturas[j] = temperaturas[j], temperaturas[i] 
                regiones[i], regiones[j] = regiones[j], regiones[i] 
                dias[i], dias[j] = dias[j], dias[i] 
                

def mostrar_temperaturas_region(temperaturas, regiones, x):
    print("Temperaturas de la región", x, ":")
    for i in range(len(regiones)):
        if regiones[i] == x:
            print(temperaturas[i])


def buscar_temperatura_mayor(temperaturas, regiones, region, x):
    for i in range(len(temperaturas)):
        if regiones[i] == region and temperaturas[i] > x:
            return True
    return False


def contar_muestras_region(regiones):
    contadores = [0] * 20
    for region in regiones:
        indice = region - 1
        contadores[indice] += 1
    return contadores


def main():
    n = int(input("Ingrese la cantidad de muestras a registrar: "))

    # Vectores
    temperaturas = [0] * n
    regiones = [0] * n
    dias = [0] * n

    cargar_datos(temperaturas, regiones, dias, n)

    # Punto 1
    promedio = calcular_promedio(temperaturas)
    print("El promedio general de temperatura es de: ", promedio, "°")

    # Punto 2
    region = int(input("Ingrese la región para mostrar temperaturas: "))
    ordenar(temperaturas, regiones, dias)
    mostrar_temperaturas_region(temperaturas, regiones, region)

    # Punto 3
    region = int(input("Ingrese la región para buscar mayores: "))
    x = float(input("Ingrese la mayor a buscar: "))
    existe_mayor = buscar_temperatura_mayor(temperaturas, regiones, region, x)
    if existe_mayor:
        print("Se encontraron temperaturas mayores a la buscada")
    else:
        print("NO se encontraron temperaturas que superen el umbral")

    # Punto 4
    cantidades = contar_muestras_region(regiones)
    for i in range(len(cantidades)):
        print("Cantidad de muestras de la region", i, ":", cantidades[i])


if __name__ == '__main__':
    main()

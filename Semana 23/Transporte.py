import camiones

def menu():
    print("1_Cargar datos")
    print("2_Listar por nombre")
    print("3_Total carga por tipo")
    print("4_Listas camiones rentables")
    print("5_Buscar por patente")
    print("6_Salir")
    return int(input("Ingrese opción: "))


def cargar_datos():
    n = int(input("Ingrese la cantidad de camiones: "))
    v = [None] * n
    for i in range(n):
        v[i] = camiones.cargar_camion()
    return v


def mostrar_vector(v):
    for i in range(len(v)):
        camiones.write(v[i])


def ordenar(v):
    n = len(v)
    for i in range(n - 1):
        for j in range(i + 1, n):
            if v[i].patente > v[j].patente:
                v[i], v[j] = v[j], v[i]


def totales_por_tipo(v):
    totales = [0] * 10
    for camion in v:
        totales[camion.tipo_producto] += camion.carga_asignada
    return totales


def listar_rentables(v):
    for camion in v:
        porcentaje = camion.carga_asignada / camion.carga_maxima
        if porcentaje > 0.75:
            camiones.write(camion)


def buscar(v, x):
    for i in range(len(v)):
        if x == v[i].patente:
            return v[i]
    return None


def main():
    op = 1
    v = []
    while op != 6:
        op = menu()
        if op == 1:
            v = cargar_datos()
        elif op == 2:
            ordenar(v)
            mostrar_vector(v)
        elif op == 3:
            totales = totales_por_tipo(v)
            for i in range(len(totales)):
                if totales[i] != 0:
                    print("Total del tipo", i, ":", totales[i])
        elif op == 4:
            listar_rentables(v)
        elif op == 5:
            x = input("Ingrese patente: ")
            res = buscar(v, x)
            if res is None:
                print("No se encontró el camion")
            else:
                conductor = input("Ingrese nuevo conductor: ")
                res.conductor = conductor
                camiones.write(res)
        elif op == 6:
            pass


if __name__ == '__main__':
    main()

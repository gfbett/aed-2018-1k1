import random

LETRAS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
DIGITOS = "0123456789"
CONDUCTORES = ["Juan", "Hugo", "Matias", "Toretto", "Brian", "Mia"]

class Camion:
    def __init__(self, patente, conductor, tipo_producto,
                 carga_maxima, carga_asignada):
        self.patente = patente
        self.conductor = conductor
        self.tipo_producto = tipo_producto
        self.carga_maxima = carga_maxima
        self.carga_asignada = carga_asignada


def write(camion):
    print("Camion: patente:{} conductor:{} tipo:{} c.max:{} c.asig:{}"
          .format(camion.patente, camion.conductor, camion.tipo_producto,
                  camion.carga_maxima, camion.carga_asignada))


def patente_random():
    patente = ""
    for i in range(3):
        patente += random.choice(LETRAS)
    patente += " "
    for i in range(3):
        patente += random.choice(DIGITOS)
    return patente


def cargar_camion():
    patente = patente_random()
    conductor = random.choice(CONDUCTORES)
    tipo = random.randint(0, 9)
    maxima = random.randint(2000, 4000)
    actual = random.randint(0, maxima)
    return Camion(patente, conductor, tipo, maxima, actual)


if __name__ == '__main__':
    x = cargar_camion()
    write(x)

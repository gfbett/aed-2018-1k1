import funciones

# Inicialización
clet = cpal = cantidad_digitos = cant_e = 0
palabras_mayusculas = palabras_e = 0
cantidad_impares = longitud_impares = 0

# Carga de texto
# texto = input("Ingrese texto: ")
texto = "Me llamo Elena y nací el 10 de febrero de 1990."
texto += "."

for letra in texto:
    if funciones.es_letra(letra):
        # Dentro de palabra
        clet += 1
        if clet == 1 and funciones.es_mayuscula(letra):
            palabras_mayusculas += 1
        if funciones.es_digito(letra):
            cantidad_digitos += 1
        if letra in ("e", "E"):
            cant_e += 1
    else:
        # Fin de palabra
        if clet > 0:
            cpal += 1
            if cant_e > 1:
                palabras_e += 1
            if funciones.es_impar(clet):
                longitud_impares += clet
                cantidad_impares += 1
        clet = cant_e = 0


print("Palabras que empiezan con mayúscula:", palabras_mayusculas)
print("Números del 0 al 9 en todo el texto:", cantidad_digitos)
print("Palabras que tienen más de una e:", palabras_e)
promedio = longitud_impares / cantidad_impares
print("Promedio de letras por palabra, para las palabras de longitud impar", promedio)

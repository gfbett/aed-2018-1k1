
def es_letra(car):
    return car != ' ' and car != '.'


def es_digito(car):
    return "0" <= car <= "9"


def es_mayuscula(car):
    return "A" <= car <= "Z"


def es_impar(num):
    return num % 2 != 0


if __name__ == '__main__':
    print("es_mayuscula A", es_mayuscula("A"))
    print("es_mayuscula a", es_mayuscula("a"))
    print("Es_digito a", es_digito("a"))
    print("Es_digito 2", es_digito("2"))
    print("Es_digito 2a", es_digito("2a"))



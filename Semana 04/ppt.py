import random

JUGADAS = ("Piedra", "Papel", "Tijera")
jugada_jugador = input("Ingrese su jugada: ")
jugada_pc = random.choice(JUGADAS)

print("El jugador eligió", jugada_jugador, "la pc", jugada_pc)

if jugada_pc == jugada_jugador:
    print("Empate")
else:
    if (jugada_jugador == "Piedra" and jugada_pc == "Tijera") or \
            (jugada_jugador == "Papel" and jugada_pc == "Piedra") or \
            (jugada_jugador == "Tijera" and jugada_pc == "Papel"):

        print("Gana el jugador")
    else:
        print("Gana la PC")

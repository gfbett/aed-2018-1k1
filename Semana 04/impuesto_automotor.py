modelo = int(input("Ingrese año de fabricación: "))
tipo = input("Ingrese tipo: ")

antiguedad = 2018 - modelo
impuestos = 0

if tipo == "R":
    impuestos = antiguedad * 100


if tipo == "P" or tipo == "T":
    if antiguedad < 10:
        impuestos = 200
    else:
        if antiguedad < 20:
            impuestos = 150

if tipo == "T":
    impuestos = impuestos + 150

print("El monto de impuestos calculado es: ", impuestos)

import acopios


def menu():
    print("1_ Generar arreglo")
    print("2_ Listar arreglo por nombre")
    print("3_ Total de acopio por tipo y mes")
    print("4_ Mes con mayor acopio")
    print("5_ Promedio de acopio por tipo de grano")
    print("6_ Salir")
    return int(input("Ingrese la opción: "))


def cargar_acopios():
    n = int(input("Ingrese la cantidad de acopios: "))
    v = [None] * n
    for i in range(n):
        v[i] = acopios.cargar_acopio_random()
    return v


def ordenar(v):
    n = len(v)
    for i in range(n - 1):
        for j in range(i + 1, n):
            if v[i].nombre > v[j].nombre:
                v[i], v[j] = v[j], v[i]


def listar_por_nombre(v):
    ordenar(v)
    for acopio in v:
        acopios.write(acopio)


def totales_por_mes_tipo(v):
    cant = [[0] * 12 for i in range(7)]
    for acopio in v:
        tipo = acopio.tipo
        mes = acopio.mes
        cant[tipo][mes - 1] += acopio.quintales
    return cant


def totales_por_mes(v):
    cant = [0] * 12
    for acopio in v:
        mes = acopio.mes
        cant[mes - 1] += acopio.quintales
    return cant


def mayor_mes(meses):
    mayor = meses[0]
    mayor_pos = 0
    for i in range(1, meses):
        if meses[i] > mayor:
            mayor = meses[i]
            mayor_pos = i
    return mayor_pos + 1


def promedio_tipo(v, x):
    suma = cantidad = 0
    for acopio in v:
        if acopio.tipo == x:
            suma += acopio.quintales
            cantidad += 1
    return suma / cantidad


def main():
    op = 0
    v = []
    while op != 6:
        op = menu()
        if op == 1:
            v = cargar_acopios()
        elif op == 2:
            listar_por_nombre(v)
        elif op == 3:
            m = totales_por_mes_tipo(v)
            for i in range(7):
                for j in range(12):
                    if m[i][j] > 0:
                        print("Cantidad del tipo", i, "mes", j, ":", m[i][j])
        elif op == 4:
            meses = totales_por_mes(v)
            mayor = mayor_mes(meses)
            print("El mes con mayor acopio es:", mayor)
        elif op == 5:
            x = int(input("Ingrese tipo de grano:"))
            promedio = promedio_tipo(v, x)
            print("El promedio del tipo", x, "es:", promedio)
        elif op == 6:
            pass
        else:
            print("Opción erronea!")


if __name__ == '__main__':
    main()


def validar_rango(mensaje, min, max):
    x = int(input(mensaje))
    while x < min or x > max:
        x = int(input("Error valor incorrecto. Ingrese nuevamente: "))
    return x


def validar_mayor(mensaje, min):
    x = int(input(mensaje))
    while x < min:
        x = int(input("Error valor incorrecto. Ingrese nuevamente: "))
    return x

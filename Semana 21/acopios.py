from validaciones import validar_mayor
from validaciones import validar_rango
import random


class Acopio:
    def __init__(self, nombre, tipo, mes, quintales):
        self.nombre = nombre
        self.tipo = tipo
        self.mes = mes
        self.quintales = quintales


def write(acopio):
    print("Acopio:", end="")
    print(" Nombre:", acopio.nombre, end="")
    print(" Tipo:", acopio.tipo, end="")
    print(" Mes:", acopio.mes, end="")
    print(" Quintales:", acopio.quintales)


def cargar_acopio():
    nombre = input("Ingrese nombre: ")
    tipo = validar_rango("Ingrese tipo (0-6): ", 0, 6)
    mes = validar_rango("Ingrese mes (1-12): ", 1, 12)
    cantidad = validar_mayor("Ingrese cantidad de quintales: ", 0)
    acopio = Acopio(nombre, tipo, mes, cantidad)
    return acopio


def cargar_acopio_random():
    nombre = random.choice("ABCDEFGHIJKLMNOPQRSTUVWXYZ")
    tipo = random.randint(0, 6)
    mes = random.randint(1, 12)
    cantidad = random.randint(100, 1000)
    acopio = Acopio(nombre, tipo, mes, cantidad)
    return acopio


if __name__ == '__main__':
    x = cargar_acopio_random()
    write(x)


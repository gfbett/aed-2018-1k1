opcion = 0
while opcion != 3:
    print("1 - Determinar primo: ")
    print("2 - Determinar múltiplos en intervalo: ")
    print("3 - Salir")
    opcion = int(input("\nIngrese opción: "))

    if opcion == 1:
        x = int(input("Ingrese un número: "))

        for i in range(2, x):
            if x % i == 0:
                print("El número no es primo")
                break
        else:
            print("El número es primo")

    elif opcion == 2:
        a = int(input("Ingrese límite inferior: "))
        b = int(input("Ingrese límite superior: "))

        cantidad = 0
        for i in range(a, b + 1):
            if i % 2 == 0:
                continue
            if i % 7 == 0:
                cantidad += 1

        print("Cantidad de impares múltiplos de 7:", cantidad)

    elif opcion == 3:
        pass
    else:
        print("Opción incorrecta.")


